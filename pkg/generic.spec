Name: %%PKG%%
Version: %%VER%%
Release: %{autorelease}
Summary: %%SUM%%
License: MIT

Source0: %%PKG%%.tar.gz

%description
%{summary}.

%prep
%autosetup -n %%PKG%%

%build
# Replace this comment by build commands
find %{buildroot} -type f | awk -F %{buildroot} '{print $2}' > %{_builddir}/%{name}/%%PKG%%.txt

%files -f %%PKG%%.txt

%changelog
%{autochangelog}
