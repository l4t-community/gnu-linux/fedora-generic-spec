#!/bin/bash
set -xe

USER=""
REPO="%%PKG%%"

git clone --recurse-submodules -j$(nproc) https://github.com/${USER}/${REPO}

cd ${REPO}
git pull --recurse-submodules -j$(nproc)
git submodule update --init --recursive
cd ..

rm -rf ${REPO}/.git ${REPO}.tar.gz
tar czf ${REPO}.tar.gz ${REPO}
rm -rf ${REPO}
