#!/bin/bash
set -xe

HERE="$(dirname -- "${BASH_SOURCE[0]}")"

[ -z $1 ] && echo "No package name" && exit 1
[ -z $2 ] && echo "No summary" && exit 1
[ -z $3 ] && echo "No version" && exit 1

cp -r ${HERE}/../pkg/ ${HERE}/../$1

cd ${HERE}/../$1/
mv generic.spec $1.spec

sed -i 's/%%PKG%%/'$1'/g; s/%%SUM%%/'$2'/g; s/%%VER%%/'$3'/g' $1.spec
sed -i 's/%%PKG%%/'$1'/g' prepare.sh
